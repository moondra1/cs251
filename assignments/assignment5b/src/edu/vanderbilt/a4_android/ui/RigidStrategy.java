package edu.vanderbilt.a4_android.ui;

import java.util.ArrayList;

import mikera.vectorz.Vector2;

public class RigidStrategy implements AggregateEntity.StepStrategy {
  /**
   * Update the position and velocity of the aggregate object in a rigid manner,
   * treating the aggregate as a single object.
   */
  @Override
  public ArrayList<Entity.Memento> stepSimulation(AggregateEntity ent, double seconds) {
    // TODO: Fill in here, if necessary
	  ArrayList<Entity.Memento> mMementoList = new ArrayList<Entity.Memento>();

        // @@ Shouldn't you use a Visitor here?

	  Vector2 myforce = new Vector2();
		Vector2 myVel = new Vector2();
		Vector2 myPos = new Vector2();

		
		for (Entity entity : Universe.instance().mEntityList) {
			myforce.add(Universe.getForce(ent, entity));
		}

		myVel = ent.getVelocity().clone();
		myVel.add(Universe.calcVelocityDelta(ent, myforce));
		myPos = Universe.calcNewPosition(ent.getPosition(), myVel, seconds);
	// now for all entites in the Aggregrate we set the velociy and position we just calculated	
		 for (Entity entity : ent.mAggregateList)
		 {
		myPos.sub(ent.getPosition()); // myPos now has the offset everyone has to move
		myPos.add(entity.getPosition()); // new position of the entity by adding offset to previous position
		mMementoList.add(entity.update().setPosition(myPos).setVelocity(myVel));
		 }
	return mMementoList;
  }
}
