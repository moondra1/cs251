package edu.vanderbilt.a4_android.ui;

import java.util.ArrayList;

import mikera.vectorz.Vector2;

public class RealisticStrategy implements AggregateEntity.StepStrategy {
  /**
   * Update the position and velocity of the aggregate entity in a realistic manner,
   * update the position of each member entity with respect to the rest of the universe.
   */

  @Override
  public ArrayList<Entity.Memento> stepSimulation(AggregateEntity ent, double seconds) {
    // TODO: Fill in here, if necessary
	  ArrayList<Entity.Memento> mMementoList = new ArrayList<Entity.Memento>();
		

        // @@ Shouldn't you use a Visitor here?


	  for (Entity entity : ent.mAggregateList)
	  {
		Vector2 myforce = new Vector2();
		Vector2 myVel = new Vector2();
		Vector2 myPos = new Vector2();

		
		for (Entity entityUni : Universe.instance().mEntityList) {
			myforce.add(Universe.getForce(entity, entityUni));
		}

		myVel = entity.getVelocity().clone();
		myVel.add(Universe.calcVelocityDelta(entity, myforce));
		myPos = Universe.calcNewPosition(entity.getPosition(), myVel, seconds);
		mMementoList.add(entity.update().setPosition(myPos).setVelocity(myVel)); 
	  }
		return mMementoList;
  }
}
