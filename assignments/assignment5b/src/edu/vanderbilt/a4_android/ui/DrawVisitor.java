package edu.vanderbilt.a4_android.ui;

import android.util.Log;

public final class DrawVisitor implements EntityVisitor {
	/** The proxy to be used for drawing images. */
	// TODO: Fill in here, if necessary
	DrawProxy mProxy;
	
	public DrawVisitor(DrawProxy proxy) {
		// TODO: Fill in here, if necessary
		mProxy=proxy;
	}
	
	@Override
	public void visit(SimpleEntity entity) {
		// TODO: Fill in here, if necessary
		Log.d("in drawing visito","visit is called");
		mProxy.draw(entity);
	}

	@Override
	public void visit(ImmobileEntity entity) {
		// TODO: Fill in here, if necessary
		mProxy.draw(entity);
	}

	@Override
	public void visit(AggregateEntity entity) {
		// TODO: Fill in here, if necessary
		mProxy.draw(entity);
	}
}
