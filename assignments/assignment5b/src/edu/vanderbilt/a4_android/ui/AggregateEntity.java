package edu.vanderbilt.a4_android.ui;

import java.util.Iterator;
import java.util.ArrayList;

import mikera.vectorz.Vector2;
import edu.vanderbilt.a4_android.ui.Entity;
import edu.vanderbilt.a4_android.ui.SimpleEntity.Memento;

public class AggregateEntity implements Entity {
  /**
   * Interface for strategeies that influence how this entity is processed for simulation steps.
   */
  public interface StepStrategy {
    public ArrayList<Entity.Memento> stepSimulation(AggregateEntity ent, double seconds);
  }

  /** The update strategy used by this entity. */
  // TODO: Filli n here, if necessary
  StepStrategy mStrategy;

  /** A container for children of the aggregate. */
  // TODO: Filli n here, if necessary
  ArrayList<Entity> mAggregateList;

  /** The name of the aggregate. */
  // TODO: Filli n here, if necessary
  String mAggregateName;

  /** Cached value of the aggregate's current position,
   * which is defined as the center of mass for this object.
   */
  // TODO: Filli n here, if necessary
  Vector2 mAggregateVel;

  /**
   * Cached value of the aggregate's current velocity,
   * which is defined as the average velocity of it's members.
   */
  Vector2 mAggregatePos;

  /**
   * Mass of the aggregate, defined as the sum of the mass
   * of all constituent elements.
   */
  // TODO: Filli n here, if necessary
  double mAggregateMass;

  public AggregateEntity (String name, StepStrategy s) {
    // TODO: Fill in here, if necessary
	  mAggregateName=name;
	  mStrategy=s;
  }

  /**
   * Provides an iterator to the children of this entity. 
   * @return
   */
  public Iterator<Entity> iterator () {
    // TODO: Fill in here, if necessary
    return mAggregateList.iterator();
  }

  /** Add a new member to this aggregate. */
  public void add (Entity child) {
    // TODO: Fill in here, if necessary
	  mAggregateList.add(child);
  }

  /** Return the name of this entity. */
  @Override
  public String getName() {
    // TODO: Fill in here, if necessary
    return mAggregateName;
  }

  /** Return the mass of this entity. */
  @Override
  public double getMass() {
    // TODO: Fill in here, if necessary
	  double mass=0;
	  for (Entity entity : mAggregateList)
	  {
		  mass =+ entity.getMass();
	  }
    return mass;
  }

  /** Return the velocity of this entity. */
  @Override
  public Vector2 getVelocity () {
    // TODO: Fill in here, if necessary
    // The average velocity is computed as the sum of:
    //    member_velocity * member_mass
    // That sum should then be divided by the total mass in the aggregate.
	  Vector2 vel = Vector2.of(0,0);
	  for (Entity entity : mAggregateList)
	  {
		vel.addMultiple(entity.getVelocity(),entity.getMass());  
	  }
	  vel = Vector2.of(vel.x/this.getMass(),vel.y/this.getMass());
    return vel;
  }

  /** Return the position of this entity. */
  @Override
  public Vector2 getPosition () {
    // TODO: Fill in here, if necessary
    // The position is defined as the center of mass of the aggregate.
    // This is calculated as the sum of:
    //  member_position * member_mass
    // That sum is divided by the total mass of the system.
	  
	  Vector2 pos = Vector2.of(0,0);
	  for (Entity entity : mAggregateList)
	  {
		pos.addMultiple(entity.getPosition(),entity.getMass());  
	  }
	  pos = Vector2.of(pos.x/this.getMass(),pos.y/this.getMass());
    return pos;
  }

  public void setStrategy(StepStrategy s) {
    // TODO: Fill in here, if necessary
	  mStrategy=s;
  }

  public StepStrategy getStrategy () {
    // TODO: Fill in here, if necessary
    return mStrategy;
  }
  
  public class Memento implements Entity.Memento {

   
    /** The new velocity of the entity. */
    // TODO: Fill in here, if necessary
	  Vector2 mAggregateNewVel;
    /** The new position of the entity */
    // TODO: Fill in here, if necessary
	  Vector2 mAggregateNewPos;
    public Entity.Memento setPosition(Vector2 pos) {
      // TODO: Fill in here, if necessary
    	mAggregateNewPos=pos;
      return this;
    }

    @Override
    public Entity.Memento setVelocity(Vector2 vel) {
      // TODO: Fill in here, if necessary
    	mAggregateNewVel=vel;
      return this;
    }

    @Override
    public void apply() {
      // TODO: Fill in here, if necessary
    	mAggregatePos=mAggregateNewPos;
    	mAggregateVel=mAggregateNewVel;
    	
    }
	  
  }

  @Override
  public Entity.Memento update() {
    // TODO: Fill in here, if necessary
	  return new Memento();
  }

  @Override
  public void accept(EntityVisitor visitor) {
    // TODO: Fill in here, if necessary
	  visitor.visit(this);
  }

  public String toString () {
    String ret = "[%s] [AGGREGATE] {\n";
    for (Entity entity : mAggregateList)
    	ret+=String.format("[%s]  [%e] [%e,%e] [%e,%e] \n",
			  entity.getName(), entity.getMass(), entity.getVelocity().x,
			  entity.getVelocity().y, entity.getPosition().x, entity.getPosition().y );
    // TODO: Loop through the entities and add them to the ret string.

    return ret + "\n}";
    
  }
}