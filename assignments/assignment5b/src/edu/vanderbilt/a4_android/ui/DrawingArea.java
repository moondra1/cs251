package edu.vanderbilt.a4_android.ui;

import java.util.ArrayList;
import java.util.Iterator;


import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;
import android.view.View;

public class DrawingArea extends View {
	/**
	 * The width and height of the view.
	 */
	int mWidth, mHeight;

	/**
	 * A proxy for drawing objects to this view.
	 */
	DrawProxy mProxy;

	public DrawingArea(Context context) {
		super(context);
	}

	/**
	 * Invalidates this view, calling the onDraw method.
	 */
	public void drawUniverse() {
		// TODO: Fill in here, if necessary
		DrawingArea.this.postInvalidate();
	}

	/**
	 * Called each time the view is invalidated.
	 */
	protected void onDraw(Canvas canvas) {
		Log.d("drawing","in on draw");
		DrawProxy proxy = new DrawProxy(getContext(), canvas, mWidth / 2,
				mHeight / 2);
		// TODO: Fill in here, if necessary
		DrawVisitor visitor = new DrawVisitor(proxy);
		ArrayList<Entity> mEntityList = Universe.instance().mEntityList;
		for (Entity entity : mEntityList) {
			entity.accept(visitor);

		}
	}

	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// TODO: Fill in here, if necessary

		mWidth = w;
		mHeight = h;

	}
}
