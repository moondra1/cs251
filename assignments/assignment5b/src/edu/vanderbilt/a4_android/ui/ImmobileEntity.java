package edu.vanderbilt.a4_android.ui;

import edu.vanderbilt.a4_android.ui.Entity;

import mikera.vectorz.Vector2;

public class ImmobileEntity implements Entity {

	/** The name of this entity. */
	// TODO: Fill in here, if necessary
	String mEntityName;

	/** The mass of this entity. */
	// TODO: Fill in here, if necessary
	double mEntityMass;

	/** The position of this entity. */
	// TODO: Fill in here, if necessary
	protected Vector2 mEntityPos;

	public ImmobileEntity(String name, double mass, Vector2 position) {
		// TODO: Fill in here, if necessary
		mEntityName = name;
		mEntityMass = mass;
		mEntityPos=position;
	}

	/** Return the name of this entity. */
	public String getName() {
		// TODO: Fill in here, if necessary
		return mEntityName;
	}

	/** Return the mass of this entity. */
	public double getMass() {
		// TODO: Fill in here, if necessary
		return mEntityMass;
	}

	public Vector2 getVelocity() {
		// TODO: Fill in here, if necessary
		return Vector2.of(0, 0);
	}

	public Vector2 getPosition() {
		// TODO: Fill in here, if necessary
		return mEntityPos;
	}

	public class Memento implements Entity.Memento {
		public Memento setPosition(Vector2 pos) {
			// TODO: Fill in here, if necessary
			// there is nothing to set so this code does not do anything but return for method chaining
			return this;
		}

		public Entity.Memento setVelocity(Vector2 vel) {
			// TODO: Fill in here, if necessary
        // @@ This implementation looks incomplete - make sure to explain it clearly.
			
			 
			return this;
		}

		public void apply() {
        // @@ This implementation looks incomplete - make sure to explain it clearly.
			// there is nothing to apply since nothing is set everything is "immobile"
			// TODO: Fill in here, if necessary
		}
	}

	public Entity.Memento update() {
		// TODO: Fill in here, if necessary
		return new Memento();
	}

	public void accept(EntityVisitor visitor) {
		// TODO: Fill in here, if necessary
		visitor.visit(this);
	}

	/** Create a string representation of this entity. */
	public String toString() {
		return String.format("[%s] [IMMOBILE] [%e] [%e,%e]", getName(),
				getMass(), getVelocity().x, getVelocity().y, getPosition().x,
				getPosition().y);
	}

}
