package edu.vanderbilt.a4_android.ui;

import java.util.ArrayList;
import java.util.Iterator;




import edu.vanderbilt.a4_android.ui.EntityVisitor;
import mikera.vectorz.Vector2;

public class StepSimulationVisitor implements EntityVisitor {
	/** Stores mementos generated during visitation of entities. */
	// TODO: Fill in here, if necessary
	ArrayList<Entity.Memento> mMementoList = new ArrayList<Entity.Memento>();

	/**
	 * The time delta which this visitor should use as the basis for the
	 * simulation step.
	 */
	// TODO: Fill in here, if necessary
	double timedelta;

	StepSimulationVisitor(double delta) {
		// TODO: Fill in here, if necessary
		timedelta = delta;
	}

	/**
	 * Update the position and velocity of the provided entity according to the
	 * internal simulation time delta.
	 */
	@Override
	public void visit(SimpleEntity ent) {
		// TODO: Fill in here, if necessary
		// what to call here
		Vector2 myforce = new Vector2();
		Vector2 myVel = new Vector2();
		Vector2 myPos = new Vector2();
		for (Entity entity : Universe.instance().mEntityList) {
			myforce.add(Universe.getForce(ent, entity));
		}
		myVel = ent.getVelocity().clone();
		myVel.add(Universe.calcVelocityDelta(ent, myforce));
		myPos = Universe.calcNewPosition(ent.getPosition(), myVel, timedelta);
		mMementoList.add(ent.update().setPosition(myPos).setVelocity(myVel));
		

	}

	@Override
	public void visit(ImmobileEntity entity) {
		// TODO: Fill in here, if necessary.

		mMementoList.add(entity.update().setPosition(entity.getPosition())
				.setVelocity(entity.getVelocity()));
	}

	@Override
	public void visit(AggregateEntity entity) {
		// TODO: Fill in here, if necessary.
	
		mMementoList = entity.getStrategy().stepSimulation(entity, timedelta);
	}
	
	public void apply() {
		
		// TODO: Fill in here, if necessary.
		for (Entity.Memento memento : mMementoList) {
			memento.apply();
		}
	}
}
