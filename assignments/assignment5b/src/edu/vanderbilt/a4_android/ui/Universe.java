package edu.vanderbilt.a4_android.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import android.util.Log;
import edu.vanderbilt.a4_android.ui.EntityVisitor;
import edu.vanderbilt.a4_android.ui.Spliterator;
import mikera.vectorz.Vector2;

class Universe implements Iterable<Entity> {
	/** A reference to the singleton instance of the Universe object. */
	// TODO: Fill in here, if necessary
	public static Universe UniRef;

	/** A list of entities in the simulation */
	// TODO: Fill in here, if necessary
	ArrayList<Entity> mEntityList = new ArrayList<Entity>();
/** Max number of thread in the thread pool*/
	private final static int MAX_THREADS = 4;
	/** Executor Service for the threads*/
	private static ExecutorService mExecutor;

	private Universe() {
		// TODO: Fill in here, if necessary
	}

	/**
	 * Return a reference to the singleton instance of Universe, constructing a
	 * new one, if necessary.
	 */
	public static Universe instance() {
		// TODO: Fill in here, if necessary

		if (UniRef == null) {
			UniRef = new Universe();
			mExecutor = Executors.newFixedThreadPool(MAX_THREADS); // needed for step simulation need only one instance so that mExecutor can be reused
		}
		return UniRef;

	}

	/**
	 * Release the universe, so instance () will return a new Universe.
	 */
	public static void release() {
		// TODO: Fill in here, if necessary.
		UniRef = null;
		mExecutor=null; // release the executor with universe
	}
	
	

	/**
	 * Add a new entity to the simulation.
	 */
	public void addEntity(Entity ent) {
		// TODO: Fill in here, if necessary
		mEntityList.add(ent);
	}

	/**
	 * Remove the identified entity from the simulation.
	 */
	public void removeEntity(String name) {
		// TODO: Fill in here, if necessary
		mEntityList.remove(getEntity(name));
	}

	/**
	 * Get the identified entity.
	 */
	public Entity getEntity(String name) {
		// TODO: Fill in here, if necessary
		Entity temp =null;
		for (Entity ent : mEntityList) {
			if (ent.getName().equals(name)){
				 temp= ent;
				 break;
				 }
		}
		return temp;
	}

	public Iterator<Entity> iterator() {
		// TODO: Fill in here, if necessary
		return mEntityList.iterator();
	}

	class UniverseSpliterator implements Spliterator<Entity> {
		// TODO: Fill in here, if necessary
		/**
		 * Current position of this interator. ;
		 */

		private int mSplitCurrent;
		/**
		 * The end of this iterator's partition.
		 */

		private int mSplitEnd;

		/**
		 * Constructs a Spliterator that covers the entire range of the Array
		 * from beginning to end.
		 */
		public UniverseSpliterator() {
			// TODO - you fill in here
			mSplitCurrent = 0;
			mSplitEnd = mEntityList.size();
		}

		/**
		 * Constructs a Spliterator that covers the given range of the Array
		 */
		private UniverseSpliterator(int pos, int end) {
			// TODO - you fill in here
			mSplitCurrent = pos;
			mSplitEnd = end;
		}

		public long estimateSize() {
			// TODO: Fill in here, if necessary
			return (long) (mSplitEnd - mSplitCurrent);
		}

		public boolean tryAdvance(EntityVisitor visitor) {
			// TODO: Fill in here, if necessary
			if (mSplitCurrent < mSplitEnd) {
				mEntityList.get(mSplitCurrent).accept(visitor);
				++mSplitCurrent;
				return true;
			} else
				// cannot advance
				return false;
		}

		public Spliterator<Entity> trySplit() {
			// TODO: Fill in here, if necessary
			int lo = mSplitCurrent; // divide range in half
			int mid = lo + (mSplitEnd - lo) / 2;
			if (lo < mid) { // split out left half
				mSplitCurrent = mid; // reset this Spliterator's origin
				return new UniverseSpliterator(lo, mid);
			} else
				// too small to split
				return null;

		}
	}

	/**
	 * Construct a spliterator covering the full range of all entities in the
	 * simulation.
	 */
	public Spliterator<Entity> spliterator() {
		// TODO: Fill in here, if necessary
		return new UniverseSpliterator();
	}
	
	/**
	 * takes spliterator and divides it into at most four spliterators 
	 * to be fed into all the threads for parallel execution
	 */
	public ArrayList<Spliterator<Entity>> splitinfour(Spliterator<Entity> UniSplit) {
		ArrayList<Spliterator<Entity>> SplitList = new ArrayList<Spliterator<Entity>>();
		Spliterator<Entity> UniSplit1 = UniSplit.trySplit();
		if (UniSplit1 != null) {
			Spliterator<Entity> UniSplit2 = UniSplit1.trySplit();
			Spliterator<Entity> UniSplit3 = UniSplit.trySplit();
			if (UniSplit2 != null)SplitList.add(UniSplit2);
			if (UniSplit3 != null)SplitList.add(UniSplit3);
			SplitList.add(UniSplit1);
			SplitList.add(UniSplit);
		}
		return SplitList;

	}

	/**
	 * Advance the simulation by the provided number of seconds.
	 */
	public void stepSimulation(final double delta) {
		// TODO: Fill in here, if necessary

		final Spliterator<Entity> UniSplit = spliterator();

		ArrayList<Spliterator<Entity>> SpliteratorList = new ArrayList<Spliterator<Entity>>();

		// Split the array to max four
		SpliteratorList = splitinfour(UniSplit);

                // @@ Nice!

		ArrayList<Callable<StepSimulationVisitor>> CallableList = new ArrayList<Callable<StepSimulationVisitor>>();
		// now go to each spliterator in the list and add a callable for each spliterator
		//We will call all these callables later together
		for (final Spliterator<Entity> split : SpliteratorList) {
			CallableList.add(new Callable<StepSimulationVisitor>() {
				public StepSimulationVisitor call() throws Exception { // calls accepts on all entites in the respective spliterator
					StepSimulationVisitor SimulationVisitor = new StepSimulationVisitor(
							delta);
					while (split.tryAdvance(SimulationVisitor)) {

					}
					return SimulationVisitor;
				}

			});
		}
		List<Future<StepSimulationVisitor>> futures = new ArrayList<Future<StepSimulationVisitor>>(); //futures for the calllables

		try {
			futures=mExecutor.invokeAll(CallableList); //Invokes all the callables and waits until all have completed
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

		for (Future<StepSimulationVisitor> future : futures) { // aplly result for all the SImulation visitor returned
			try {
				future.get().apply();
			} catch (InterruptedException e) {

				e.printStackTrace();
			} catch (ExecutionException e) {

				e.printStackTrace();
			}
		}

	}

	private static final double mGravitationalConstant = 6.67428e-11;

	/**
	 * Calculates the force exerted on the 'base' by 'other'.
	 */
	public static Vector2 getForce(Entity other, Entity base) {
		// To be provided, do not edit.
		if (base.getPosition().equals(other.getPosition()))
			return new Vector2();
		Vector2 direction = base.getPosition().clone();
		direction.sub(other.getPosition());
		Vector2 result = direction.toNormal();
		result.scaleAdd(
				(base.getMass() * other.getMass())
						/ direction.magnitudeSquared(), 0);
		result.scaleAdd(mGravitationalConstant, 0);
		return result;
	}

	/**
	 * Calculates the change in velocity of a given Entity given a net force
	 * acting on the object.
	 */
	public static Vector2 calcVelocityDelta(Entity ent, Vector2 force) {
		// To be provided, do not edit.
		Vector2 result = force.clone();
		result.scaleAdd(1 / ent.getMass(), 0);
		return result;
	}

	public static Vector2 calcNewPosition(Vector2 position, Vector2 velocity,
			double time) {
		// To be provided, do not edit.
		Vector2 result = position.clone();
		result.addMultiple(velocity, time);
		return result;
	}

}
