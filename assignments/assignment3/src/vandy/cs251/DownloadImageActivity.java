package vandy.cs251;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

/**
 * An Activity that downloads an image, stores it in a local file on the local
 * device, and returns a Uri to the image file.
 */
public class DownloadImageActivity extends Activity {
	/**
	 * Debugging tag used by the Android logger.
	 */
	private final String TAG = getClass().getSimpleName();

	

	/**
	 * Hook method called when a new instance of Activity is created. One time
	 * initialization code goes here, e.g., UI layout and some class scope
	 * variable initialization.
	 *
	 * @param Bundle
	 *            object that contains saved state information.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// Always call super class for necessary
		// initialization/implementation.
		// @@ TODO -- you fill in here.
		super.onCreate(savedInstanceState);

		// Get the URL associated with the Intent data.
		// @@ TODO -- you fill in here.

		Uri data = this.getIntent().getData();
		new ImageDownloader().execute(data);

		// Download the image in the background, create an Intent that
		// contains the path to the image file, and set this as the
		// result of the Activity.

		// @@ TODO -- you fill in here. Note that grad students must
		// use the Android AsyncTask framework to implement this
		// logic, whereas ugrad students are free to use the Android
		// "HaMeR" framework.
	}

	/**
	 * extends AsyncTask for providing necessary functions for running a
	 * background thread
	 * 
	 * @ Arul
	 */
	private class ImageDownloader extends AsyncTask<Uri, Void, Uri> {
		/**
		 * Function which implements the background thread which does the main
		 * task
		 * 
		 * @param params
		 *            URL in string to the image
		 * 
		 * @return Uri of the downloaded image
		 * 
		 *         (non-Java doc)
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
		@Override
		protected Uri doInBackground(Uri... params) {
			Log.i(TAG, "the background is running now");
			return DownloadUtils.downloadImage(getBaseContext(), params[0]);

		}

		/**
		 * Function implementing the task to be done after the image is
		 * downloaded. Makes and intent to be sent back to the main activity
		 * 
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPostExecute(Params)
		 * 
		 * @param Savedimage
		 *            Uri of the saved image
		 * 
		 * @return void
		 * 
		 */
		@Override
		protected void onPostExecute(Uri savedImage) {
			Log.i(TAG, "OnPostExecute this");
			if (savedImage == null) {
				setResult(RESULT_CANCELED);
				Log.i(TAG,
						"Download via Aysntask failed download image returned null");
			} else {
				Intent replyIntent = new Intent();
				replyIntent.setData(savedImage);
				setResult(RESULT_OK, replyIntent);
				Log.i(TAG, "Download via AyscTask is done");
			}

			finish();
		}

		// @@ Why are you writing this code - just use
		// DownloadUtils.downloadImage()!
		//resolved

	}
}
