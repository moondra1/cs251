public CharArray(int size) {
// Checking for sze <0 was unneccesary as exception is thrown automatically

}
---------------------------------------------------------------------------

public CharArray(int size, char mDefaultvalue) {

//Didnot use the previous constructor making redundant code.Called the previous constructor to make the array.

}
---------------------------------------------------------------------------
public Object clone() {
//Did not use the Copy constructor already made.Corrected the mistake 

}
---------------------------------------------------------------------------
public void resize(int size) {
//Initially had three conditions as follows.
//1. If size is less than logical size
//2. If size is between logical size and capacity
//3.If Size is more than capacity
// Turns out that the first two condition are redundant as they perform almost the same work.
//Restructured to avoid repeating code.
}

---------------------------------------------------------------------------
public int compareTo(CharArray s) {

//Initially was checking the length first and then the alphabetical order.Corrected the mistake.
//Had used too many variables  for the for loop , used math.min and for a simplified code.

}
---------------------------------------------------------------------------
public boolean hasNext() {
// Too many redundant checks.Highly simplified by using a simple return statement 

}