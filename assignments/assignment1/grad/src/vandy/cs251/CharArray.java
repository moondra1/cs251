// @@ Make sure to delete the "ugrad" (or "grad") directory several levels up.
package vandy.cs251;

import java.lang.ArrayIndexOutOfBoundsException;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Provides a wrapper facade around primitive char arrays, allowing for dynamic
 * resizing.
 */
public class CharArray implements Comparable<CharArray>, Iterable<Character>,
		Cloneable {
	/**
	 * The underlying array.
	 */
	// TODO - you fill in here
	private char mArray[];
	/**
	 * The current size of the array.
	 */
	// TODO - you fill in here
	private int mArraySize;
	/**
	 * Default value for elements in the array.
	 */
	// TODO - you fill in here
	private char mValue;

	/**
	 * Constructs an array of the given size.
	 *
	 * @param size
	 *            Non-negative integer size of the desired array.
	 */
	public CharArray(int size) {
		// TODO - you fill in here
		// @@ Is this necessary?

		// @Arul removed size <0 checking since negative exception is thrown
		// automatically as run time exception
		this.mArray = new char[size];
		this.mArraySize = size;
	}

	/**
	 * Constructs an array of the given size, filled with the provided default
	 * value.
	 *
	 * @param size
	 *            Nonnegative integer size of the desired array.
	 * @param mDefaultvalue
	 *            A default value for the array.
	 */
	public CharArray(int size, char mDefaultvalue) {
		// TODO - you fill in here
		// @@ Make sure to reuse the other CharArray constructor to implement
		// this one.
		// @corrected the mistake
		this(size);
		this.mValue = mDefaultvalue;
		Arrays.fill(this.mArray, this.mValue);
	}

	/**
	 * Copy constructor; creates a deep copy of the provided CharArray.
	 *
	 * @param s
	 *            The CharArray to be copied.
	 */
	public CharArray(CharArray s) {
		// TODO - you fill in here
		this.mArraySize = s.mArraySize;
		this.mValue = s.mValue;
		// Consider using copyOf
		this.mArray = Arrays.copyOf(s.mArray, this.mArraySize);
	}

	/**
	 * Creates a deep copy of this CharArray. Implements the Prototype pattern.
	 */
	@Override
	public Object clone() {
		// TODO - you fill in here (replace return null with right
		// implementation).
		// @@ Why aren't you using the copy constructor?
		// @Arul Corrected the mistake using the already written copy
		// constructor
		return new CharArray(this);
	}

	/**
	 * @return The current size of the array.
	 */
	public int size() {
		// TODO - you fill in here (replace return 0 with right
		// implementation).
		return this.mArraySize;
	}

	/**
	 * @return The current maximum capacity of the array.
	 */
	public int capacity() {
		// TODO - you fill in here (replace return 0 with right
		// implementation).
		return this.mArray.length;
	}

	/**
	 * Resizes the array to the requested size.
	 *
	 * Changes the capacity of this array to hold the requested number of
	 * elements. Note the following optimizations/implementation details:
	 * <ul>
	 * <li>If the requests size is smaller than the current maximum capacity,
	 * new memory is not allocated.
	 * <li>If the array was constructed with a default value, it is used to
	 * populate uninitialized fields in the array.
	 * </ul>
	 * 
	 * @param size
	 *            Nonnegative requested new size.
	 */
	public void resize(int size) {
		// TODO - you fill in here
		if (size < 0)
			throw new IndexOutOfBoundsException();
		if (size <= this.capacity()) {
			if (size >= mArraySize)
				Arrays.fill(this.mArray, this.mArraySize, size, this.mValue);
				// copying the default values to increased size
			this.mArraySize = size;
			// @@ Could you structure this function to only have this line of
			// @@ code once?
			// @@ Isn't the condition below a little redundant?
			// @Arul COrrected redundant conditional checks
		} else if (this.capacity() < size) {
			// @@ Consider using copyOf to simplify this greatly!
			// @Arul removed unnecessary temporary value.
			// @Arul Increase size need copying value to a new array with
			// increased size
			this.mArray = Arrays.copyOf(this.mArray, size);
			Arrays.fill(this.mArray, this.mArraySize, this.capacity(),
					this.mValue);
			this.mArraySize = size;
		}
	}

	/**
	 * @return the element at the requested index.
	 * @param index
	 *            Nonnegative index of the requested element.
	 * @throws ArrayIndexOutOfBoundsException
	 *             If the requested index is outside the current bounds of the
	 *             array.
	 */
	public char get(int index) {
		this.rangeCheck(index);
		return this.mArray[index];
	}

	/**
	 * Sets the element at the requested index with a provided value.
	 * 
	 * @param index
	 *            Nonnegative index of the requested element.
	 * @param value
	 *            A provided value.
	 * @throws ArrayIndexOutOfBoundsException
	 *             If the requested index is outside the current bounds of the
	 *             array.
	 */
	public void set(int index, char value) {
		// TODO - you fill in here (replace return '\0' with right
		// implementation).
		this.rangeCheck(index);
		this.mArray[index] = value;
	}

	/**
	 * Compares this array with another array.
	 * <p>
	 * This is a requirement of the Comparable interface. It is used to provide
	 * an ordering for CharArray elements.
	 * 
	 * @return a negative value if the provided array is "less than" this array,
	 *         zero if the arrays are identical, and a positive value if the
	 *         provided array is "greater than" this array.
	 */
	@Override
	public int compareTo(CharArray s) {
		// TODO - you fill in here (replace return 0 with right
		// implementation).
		// @@ Why do you need all of these variables?
		// @Arul Simplified the function greatly using math.min and direct
		// returning values.

		for (int i = 0; i < Math.min(this.size(), s.size()); i++) {
			// Alphabetical	check till the	smaller array size
			if (this.mArray[i] != (s.mArray[i])) {
				return this.mArray[i] - s.mArray[i];
			}
		}
		return this.size() - s.size();
		// if alphabetically same but difference in lengths.Reurn zero of
		// exactly same

	}

	/**
	 * Throws an exception if the index is out of bound.
	 */
	private void rangeCheck(int index) {
		// TODO - you fill in here (replace return '\0' with right
		// implementation).
		if (index < 0 || index >= this.mArraySize) {
			throw new ArrayIndexOutOfBoundsException();
		}
	}

	/**
	 * Define an Iterator over the CharArray.
	 */
	public class CharArrayIterator implements java.util.Iterator<Character> {
		/**
		 * Keeps track of how far along the iterator has progressed.
		 */
		// TODO - you fill in here
		private int mIndex;

		/**
		 * Constructor.
		 */
		public CharArrayIterator() {
			// TODO - you fill in here
			mIndex = 0;
		}

		/**
		 * @return true if there are any remaining elements that haven't been
		 *         iterated through yet; else false.
		 */
		@Override
		public boolean hasNext() {
			// TODO - you fill in here (replace return false with
			// right implementation)
			// @@ This condition is WAY more complicated than it needs to be!
			// @@ Moreover, just way 'return ...'
			// @Arul Corrected as suggested removing redundant conditions
			return mIndex < mArraySize;
		}

		/**
		 * @return The next element in the iteration.
		 */
		@Override
		public Character next() {
			// TODO - you fill in here (replace return '\0' with right
			// implementation)
			return mArray[mIndex++];
		}
	}

	/**
	 * Factory method that returns an Iterator.
	 */
	public Iterator<Character> iterator() {
		// TODO - you fill in here (replace return null with right
		// implementation)
		return new CharArrayIterator();
	}
}
